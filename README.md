# Pakrat

Shell script to create Bash aliases for Flatpaks so you can launch them from a shell.

## Create an alias

Run this command after installing a Flatpak you think you might want to launch from a shell at some point:

```
$ flatpak install org.gnu.emacs
$ pakrat add -c 'flatpak run org.gnu.emacs' emacs
Alias for emacs added.
Run this command to update your shell:
. ~/.bashrc.d/flatpak_aliases

$ . ~/.bashrc.d/flatpak_aliases
```

## Launch

Now you can launch your application from a shell:

```
$ emacs
```

## List

List existing aliases:

```
$ pakrat list
alias emacs='flatpak run org.gnu.emacs'
```

## Remove

Remove an alias:

```
$ pakrat remove emacs
```

This doesn't remove the Flatpak and only operates on the dedicated ``flatpak_aliases`` file.

## Load aliases

All ``pakrat`` aliases are added to ``~/.bashrc.d/flatpak_aliases``, which you can automatically source when your shell is launched by placing this code into your ``.bashrc`` or ``.bash_profile`` file (assuming it's not already there):

```
if [ -d ~/.bashrc.d ]; then
  for rc in ~/.bashrc.d/*; do
    if [ -f "$rc" ]; then
      . "$rc"
    fi
  done
fi

unset rc
```
